package com.mazapps.template.data.network

import com.mazapps.template.api.RetrofitApiService
import com.mazapps.template.data.model.Wikipedia
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author morad.azzouzi on 01/12/2019.
 */
@Singleton
open class AppApiHelper @Inject constructor(
    private val retrofit: RetrofitApiService
) : ApiHelper {

    override fun getCount(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Single<Response<Wikipedia>> = retrofit.getCount(action, format, list, keyword)
}