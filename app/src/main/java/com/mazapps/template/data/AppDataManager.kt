package com.mazapps.template.data

import com.mazapps.template.data.db.DbHelper
import com.mazapps.template.data.model.Search
import com.mazapps.template.data.model.Wikipedia
import com.mazapps.template.data.network.ApiHelper
import io.reactivex.Single
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

/**
 * @author morad.azzouzi on 01/12/2019.
 */
@Singleton
open class AppDataManager @Inject constructor(
    private val apiHelper: ApiHelper,
    private val dbHelper: DbHelper
) : DataManager {

    override fun getCount(
        action: String,
        format: String,
        list: String,
        keyword: String
    ): Single<Response<Wikipedia>> = apiHelper.getCount(action, format, list, keyword)

    override fun insert(search: Search): Single<Long> = dbHelper.insert(search)

    override fun fetchRelatedSearch(): Single<List<Search>> = dbHelper.fetchRelatedSearch()
}