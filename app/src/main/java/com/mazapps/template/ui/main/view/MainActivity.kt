package com.mazapps.template.ui.main.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import android.widget.Toast.LENGTH_SHORT
import androidx.appcompat.app.AppCompatActivity
import com.mazapps.template.R
import com.mazapps.template.data.CallStateEnum
import com.mazapps.template.ui.main.viewmodel.MainViewModel
import dagger.hilt.android.AndroidEntryPoint
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject lateinit var viewModel: MainViewModel
    @Inject lateinit var disposables: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        observeState()
        initView()
    }

    private fun observeState() {
        disposables.add(
            viewModel
                .state
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when (it) {
                        CallStateEnum.SUCCESS -> onGetKeywordCountSuccess()
                        CallStateEnum.ERROR -> onGetKeywordCountError()
                        else -> {
                            // do nothing
                        }
                    }
                }
        )

        disposables.add(
            viewModel
                .fetchState
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    when (it) {
                        CallStateEnum.SUCCESS -> onFetchRelatedSearchSuccess()
                        CallStateEnum.ERROR -> onFetchRelatedSearchError()
                        else -> {
                            // do nothing
                        }
                    }
                }
        )
    }

    private fun onGetKeywordCountSuccess() {
        count.text = getString(R.string.count, viewModel.count.toString())
        button.visibility = View.VISIBLE
    }

    private fun onGetKeywordCountError() {
        Toast.makeText(this, getString(R.string.error_occured), LENGTH_SHORT).show()
    }

    private fun onFetchRelatedSearchSuccess() {
        Toast.makeText(
            this,
            getString(R.string.count, viewModel.relatedSearchItemsSize.toString()),
            LENGTH_SHORT
        ).show()
    }

    private fun onFetchRelatedSearchError() {
        Toast.makeText(this, getString(R.string.error_occured), LENGTH_SHORT).show()
    }

    private fun initView() {
        button.setOnClickListener { disposables.add(viewModel.fetchRelatedSearch()) }
    }

    override fun onStart() {
        super.onStart()
        disposables.add(viewModel.getKeywordCount())
    }

    override fun onDestroy() {
        disposables.clear()
        super.onDestroy()
    }
}